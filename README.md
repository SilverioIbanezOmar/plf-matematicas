# Omar Silverio Ibañez PLF ISA
## Mapa Conceptual Conjuntos, Aplicaciones y funciones (2002)
```plantuml
@startmindmap
<style>
mindmapDiagram {
  .node {
    BackgroundColor lightblue
  }  
  :depth(1){
    BackgroundColor green
  }
  :depth(2){
    BackgroundColor yellow
  }
  :depth(3){
    BackgroundColor yellow
  }
}
</style>
*[#Red]: Conjuntos
 Aplicaciones y funciones;
**[#Green] Conjuntos
***_ incluye
**** Conjunto
**** Elemento
**** Ambas no tienen una definición
*****_ porque
****** Cualquier definición seria similar
****** Es una idea intuitiva
****** Es asumida por el observador
**** La relación entre ambas
*****_ es de
****** Pertenencia
**** Se sabe si esta o no
***** un elemento dentro de un conjunto
*** Inclusión de conjuntos
****_ es
***** Primera noción entre conjuntos
***** Dentro de un conjunto hay mas conjuntos
***** Elementos de un conjunto
******_ pertenece
******* otro conjunto
*** Conjunto elementales
**** Son conjuntos mas complejos
*** Operaciones con conjuntos
****_ son
***** Intersección
***** Unión
***** Complementación
***** Diferencia de Conjuntos
*** Tipos de conjuntos
**** Conjunto universal
*****_ es
****** Conjunto de referencia
**** Conjunto vacío
*****_ es
****** Conjunto que no tiene elementos
**** Representación
*****_ mediante
****** Diagramas de Venn
******* Ayudan a comprender los conjuntos
******* Representa las operaciones de los conjuntos
*******_ es
******** Comada
******** Intuitiva
******** Util
******* No sirve para definir
**** Cardinalidad de conjuntos
*****_ une
****** Teoria de los numeros naturales
****** Teoria de conjuntos
***** Numero de elementos que conforman un conjunto 
***** Propiedad de las cardinalidades
******_ son
******* Unión cardinal de conjuntos
******** es igual a la cardinalidad de uno de los conjuntos
******** Formula
********* (Conjunto A + Conjunto B) - Interseccion de los elementos comunes
******* Acotación de cardinales
******** Se encuentra una serie de cardinales
*********_ sobre
********** Mayor
********** Menor
********** Igual

** Aplicación
***_ es
**** Transformación
**** Regla
**** Cambio
*** Convierte
*** Composición de aplicaciones
****_ indica
***** Transformación de una transformación
***** O Primero una aplicación
****** Despues Otra

** Función
*** Conjuntos que se transforman
*** Grafica de la función
**** Son mas faciles de ver
**** Conjunto de puntos en un plano
@endmindmap
```
---
## Funciónes (2010)
```plantuml
@startmindmap
<style>
mindmapDiagram {
  .node {
    BackgroundColor lightblue
  }  
  :depth(1){
    BackgroundColor green
  }
  :depth(2){
    BackgroundColor yellow
  }
  :depth(3){
    BackgroundColor yellow
  }
}
</style>
*[#Red] Funciones
** Corazón de la matematicas
**_ reflega
*** Pensamiento del hombre
**_ es
*** Situación de cambio
*** El momento produce cambios
*** Conocer como se aplica las matematicas
****_ al
***** Cambio
**_ Definición
*** Transformaciones de un conjunto a otro
*** Aplicación especial
*** Conjuntos que se relacionan
****_ ven
***** Sus cambios
***** Conjunto de numeros
** Representación cartesiana
***_ es
**** Representación grafica de una función
**** Se representa en un plano
*****_ se pone
****** su numero
****** su imagen
**** Con valores numericos reales
** Función creciente
***_ crece
**** el valor de la variable independiente
**** el valor de la función
** Función decreciente
***_ aumenta
**** el valor de la variable independiente
***_ disminuye
**** el valor de la función
** Maximos relativos
***_ si la ordenada es
**** Mayor o igual
***** Punto del dominio de la función
** Minimo Realitvo
***_ si la ordenada es
**** Menor o igual
***** Punto del dominio de la función
** Limite
*** Comportamiento local de las funciones
***_ se considera
**** valores de la variables
*****_ estan
****** Cerca del punto 
****** Cerca de un determinado valor
***_ significa
**** es continuo
**** no tiene saltos
*****_ excepto
****** Valores 0
***_ carecen de
**** Limite
***** Si tienden a 0
** Continuidad
***_ No produce
**** saltos
**** descontinuidades
***_ son
**** Funciones manejables
** Derivada
***_ Nace de 
**** funciones sencillas
**** comportamientos complicados 
*** Pasan de lo complicado a lo sencillo
***_ Resuleve la
**** Aproximación de una función
@endmindmap
```
## La matemática del computador (2002)
```plantuml
@startmindmap
<style>
mindmapDiagram {
  .node {
    BackgroundColor lightblue
  }  
  :depth(1){
    BackgroundColor green
  }
  :depth(2){
    BackgroundColor yellow
  }
  :depth(3){
    BackgroundColor yellow
  }
}
</style>
*[#Red] La matematica del computador
** Aritmetica del computador
*** Calculo con numeros abstractos
*** abstracción de numeros reales
** Aproxima numeros
*** Reales
*** fraccionarios
**_ problemas
*** idea de numero aproximado
*** idea de error aproximado
**** Numero real
**** Numero finito de cifras
*** No podemos representar cifras infinitas
****_ tecnicas para resolverlo
***** Truncamiento
****** Cortar el numero sin mas
****** Desprecio el numero
***** Redondeo
****** Cortar el numero
****** Intenta que el error
*******_ sea la
******** Media
** Digitos significativos
*** Numeros
****_ que proporcionan
***** Información de magnitud
*** Numeros grandes y muy pequeños
****_ se representan
***** Notación cientifica
***** Exponencial normalizada
****_ permite
***** Observar la magnitud
***** Conservar un orden
** Sistema binario
***_ representan
**** Pulsos electricos
***_ son de
**** Simple composición
***** 0
***** 1
***_ encaja
**** Logica booleana
**** Pertenecia de conjuntos
***_ es en
**** Base 2
** Materialización fisica
***_ consiste en
**** Convertir Binario
*****_ en posiciones de
****** Memoria
**** Convertir
**** concepto abstracto
****_ Representa por
***** Circuito de corrientes
** Representación de numeros
*** Codificación binaria
*** Numeros con grandes simbolos binarios
****_ se convierten
***** Sistema octal
***** Sistema hexadecimal
@endmindmap
```
